
/**
 * The purpose of this class is to test the functionality of the Employee class.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class EmployeeTester {

	/**
	 * Within the main method of the class a new employee object is created and get/set methods are used to modify the attributes of the employee object.
	 * Finally the employee object is printed to the console in string format to show the changes made to the object.
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		// Creating a new employee.
		System.out.println("New Employee Object: ");
		Employee empTest = new Employee("Ronald McDonald", "M", "06-03-1962", "01 The Golden Arches", 
				                        "MD1 7FF", 505, "Sales", "02-01-1972", 3400, "Ronald.McDonald@Nmail.com");
		
		// Prints the new employee object to the console.
		System.out.println(empTest);
	    System.out.println();
	    
	    
	    System.out.println("Modifying employee object: ");
	    System.out.println();
	    
	    
	    // Sets the new name of the employee and the prints the new name to the console.
		empTest.setName("Amy Larsson");
		System.out.println("New employee name: ");
		System.out.println(empTest.getName());
		System.out.println();
		
		// Sets the new gender of the employee and the prints the new gender to the console.
		empTest.setGender("F");
		System.out.println("New employee gender: ");
		System.out.println(empTest.getGender());
		System.out.println();
		
		// Sets the new date of birth of the employee and the prints the new date of birth to the console.
		empTest.setDob("09-12-1993");
		System.out.println("New employee date of birth: ");
		System.out.println(empTest.getDob());
		System.out.println();
		
		// Sets the new address of the employee and the prints the new address to the console.
		empTest.setAddress("43 Station Road");
		System.out.println("New employee address: ");
		System.out.println(empTest.getAddress());
		System.out.println();
		
		// Sets the new post code of the employee and the prints the new post code to the console.
		empTest.setPostcode("CT4 8SK");
		System.out.println("New employee postcode: ");
		System.out.println(empTest.getPostcode());
		System.out.println();
		
		// Sets the new employee number of the employee and the prints the new employee number to the console.
		empTest.setEmployeeNumber(99);
		System.out.println("New employee, employee number: ");
		System.out.println(empTest.getEmployeeNumber());
		System.out.println();
		
		// Sets the new department of the employee and the prints the new department to the console.
		empTest.setDepartment("HR");
		System.out.println("New employee department: ");
		System.out.println(empTest.getDepartment());
		System.out.println();
		
		// Sets the new start date of the employee and the prints the new start date to the console.
		empTest.setStartDate("01-01-2017");
		System.out.println("New employee start date: ");
		System.out.println(empTest.getStartDate());
		System.out.println();
		
		// Sets the new salary of the employee and the prints the new salary to the console.
		empTest.setSalary(22000);
		System.out.println("New employee salary: ");
		System.out.println(empTest.getSalary());
		System.out.println();
		
		// Sets the new email of the employee and the prints the new email to the console.
		empTest.setEmail("Amy.Larsson@Nmail.com");
		System.out.println("New employee email: ");
		System.out.println(empTest.getEmail());
		System.out.println();
		
		// Prints out the employee after the modifications to show the methods are working correctly.
		System.out.println("Employee after attribute modification:  ");
		System.out.println(empTest);
		
		

	}

}
