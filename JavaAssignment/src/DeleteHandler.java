import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.HashMap;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * The purpose of this class is to handle the deleteEmployee request and to respond to that request by deleting the appropriate employee from the database
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class DeleteHandler implements HttpHandler {

	/**
	 * This method reads in the employee number in hashmap format, converts the input into string format and passes the value of the key/value pair into a String.
	 * The String is then passed into the parameter of the deleteEmployee method attached to an employeeDAO object, which in turn runs the method and deletes the employee from the database.
	 * @exception IOException - This exception catches any potential io error that may occur from running the method.
	 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when deleting the employee from the database.
	 */
		
		public void handle(HttpExchange httpEx) throws IOException {
			System.out.println();
			System.out.println("Deleting Employee: ");
			HashMap<String, String> delete = new HashMap <String, String>();
			EmployeeDAO empDao = new EmployeeDAO();
			
			//Reading the request body.
			BufferedReader in = new BufferedReader(new InputStreamReader(httpEx.getRequestBody()));
			String line ="";
			String request = "";
			while ((line = in.readLine()) != null) {
				request = request + line;
			}
			System.out.println(">>>>>>>>>> "+request);
			
			
			String [] pairs = request.split("&");
			
			for (int i = 0; i<pairs.length; i++) {
				String pair = pairs[i];
				
				delete.put(URLDecoder.decode(pair.split("=")[0],"UTF-8"),URLDecoder.decode(pair.split("=")[1],"UTF-8"));
			}
			
			// HashMap of posted data is now contained in the "delete" variable.


			String del = delete.get("EmployeeNumber");
		
			
			System.out.println(del);

			
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(httpEx.getResponseBody()));					
			  
			
			try { 
				empDao.deleteEmployee(del); 
				httpEx.sendResponseHeaders(200, 0); //HTTP 200 (OK)
				out.write("Success!");
				out.close();
			}
			catch(SQLException se) {
				 httpEx.sendResponseHeaders(500, 0); //HTTP 500 (Internal Server Error)
				 out.write("Error Deleting Employee");
				 out.close();
			}
		

	}

}
