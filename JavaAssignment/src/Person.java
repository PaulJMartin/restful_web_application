
/**
 * The purpose of this class is to construct, modify and return a person object.
 * 
 * @author Paul
 * @version 1.0
 */


public class Person {
	
	private String name;
	private String gender;
	private String dob;
	private String address;
	private String postcode;
	
	/**
	 * Constructor method for constructing a person object. The parameters are assigned to the relevant instance variables within the method body.
	 * @param n  Constructs the name of the person object
	 * @param g  Constructs the gender of the person object
	 * @param dateOfBirth  Constructs the date of birth of the person object
	 * @param add  Constructs the address of the person object
	 * @param pCode  Constructs the post code of the person object
	 */
	
	public Person(String n, String g, String dateOfBirth, String add, String pCode) 
	{
		this.name = n;
		this.gender = g;
		this.dob = dateOfBirth; 
		this.address = add;
		this.postcode = pCode;
		
	}
	
	/**
	 * Method for returning the name of the person object
	 * @return Returns the value of the name variable
	 */
	
	public String getName()
	{
        return this.name;
	}
	
	/**
	 * Method for returning the gender of the person object
	 * @return Returns the value of the gender variable
	 */
	
	public String getGender()
	{
		return this.gender;
	} 
	
	/**
	 * Method for returning the date of birth of the person object
	 * @return Returns the value of the date of birth variable
	 */
	
	public String getDob()
	{
		return this.dob;
	}
	
	/**
	 * Method for returning the address of the person object
	 * @return Returns the value of the address variable
	 */
	
	public String getAddress()
	{
		return this.address;
	}
	
	/**
	 * Method for returning the post code of the person object
	 * @return Returns the value of the post code variable
	 */
	
	public String getPostcode()
	{
		return this.postcode;
	}
	
	
	
	/**
	 * Method for modifying the name of the person object
	 * @param setn  Accepts a string input from the user and sets the name variable to the new value
	 */
	
	
	public void setName(String setn)
	{
		this.name = setn;
	}
	
	/**
	 * Method for modifying the gender of the person object
	 * @param setg  Accepts a string input from the user and sets the gender variable to the new value
	 */
	
	public void setGender (String setg)
	{
		this.gender = setg;
	}
	
	/**
	 * Method for modifying the date of birth of the person object
	 * @param setdob  Accepts a string input from the user and sets the dob variable to the new value
	 */
	
	public void setDob(String setdob)
	{
		this.dob = setdob;
	}
	
	
	/**
	 * Method for modifying the address of the person object
	 * @param setadd  Accepts a string input from the user and sets the address variable to the new value
	 */
	
	public void setAddress (String setadd)
	{
		this.address = setadd;
	}
	
	/**
	 * Method for modifying the post code of the person object
	 * @param setpcode  Accepts a string input from the user and sets the post code variable to the new value
	 */
	
	public void setPostcode (String setpcode)
	{
		this.postcode = setpcode;
	}
	
	
	/**
	 * Method for returning a string representation of the person object
	 * @return - Returns a string which includes a description of the variables and the values of the variables from the person object.
	 */
	
	public String toString()
	{
		return "Employee name: "+this.getName()+", Gender: "+this.getGender()+", Date Of Birth: "+this.getDob()+", Address: "+this.getAddress()
		+", Postcode: "+this.getPostcode();
		
	}
	

}
