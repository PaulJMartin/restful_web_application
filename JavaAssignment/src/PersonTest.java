
/**
 * The purpose of this class is to test the functionality of the Person class
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class PersonTest {

	/**
	 * Within the main method of the class a new person object is created and get/set methods are used to modify the attributes of the person object.
	 * Finally the person object is sent back to the console in string format to show the changes made to the object.
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		// Creating a new person.
		System.out.println("New Person Object: ");
		Person perTest = new Person("Andy Williams", "M", "25-12-1982", "34 Park View Lane", "PV1 7GJ");
		
		// Prints the new person object to the console.
		System.out.println(perTest);
	    System.out.println();
	    
	    
	    System.out.println("Modifying Person Object: ");
	    System.out.println();
	    
	    
	    // Sets the new name of the person and the prints the new name to the console.
	    perTest.setName("Amy Larsson");
		System.out.println("New person name: ");
		System.out.println(perTest.getName());
		System.out.println();
		
		
		// Sets the new gender of the person and the prints the new gender to the console.
		perTest.setGender("F");
		System.out.println("New person gender: ");
		System.out.println(perTest.getGender());
		System.out.println();
		
		
		// Sets the new date of birth of the person and the prints the new date of birth to the console.
		perTest.setDob("09-12-1993");
		System.out.println("New person date of birth: ");
		System.out.println(perTest.getDob());
		System.out.println();
		
		
		// Sets the new address of the person and the prints the new address to the console.
		perTest.setAddress("43 Station Road");
		System.out.println("New person address: ");
		System.out.println(perTest.getAddress());
		System.out.println();
		
		
		// Sets the new post code of the person and the prints the new post code to the console.
		perTest.setPostcode("CT4 8SK");
		System.out.println("New person postcode: ");
		System.out.println(perTest.getPostcode());
		System.out.println();

		
		// Prints out the person after the modifications, to show the methods are working correctly.
		System.out.println("Person after attribute modification:  ");
		System.out.println(perTest);
		
		
	}
	

}
