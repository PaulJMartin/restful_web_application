import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import com.google.gson.Gson;

/**
 * The purpose of this class is to test the functionality of all the HTTP handler objects.
 * @author Paul Martin, Student Number: 17097016
 * @version 1.0
 */

public class WebServiceTester {
	
	/**
	 * Within the main method of this class, all the HTTP handler functions are tested through calling the methods associated each handler test.
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 * @exception Exception - Uninitialised exception, if initialised an error will be returned to the user.
	 */

	public static void main(String[] args) throws IOException {

		try {
			
			System.out.println("Employees = " + getEmployees());  // The getEmployees method is used here to show all the employees currently in the database. 
			
			insertEmployee(); // The insertEmployee method is used here to test the functionality of the insert method.
			
			getSpecificEmployee(); // The getSpecificEmployee method is used here to test the insertEmployee method by attempting to retrieve the newly inserted employee. 
			
			UpdateEmployeeSalary(); // The updateEmployeeSalary is used here to test the functionality of the update method.
			
			getSpecificEmployee(); // The getSpecificEmployee method is used here to test that the update method is working correctly, by retrieving the newly updated employee. 
		    
		    deleteEmployee(); // The deleteEmployee method is used here to test the functionality of the delete method.
		    
		    getSpecificEmployee(); /* The getSpecificEmployee method is used here to test that the delete method is working correctly, by attempting to retrieve the recently deleted employee.
			                          If the delete method is working correctly, the statement should return a NULL value. */
		    
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Tests the functionality of the get-json handler and returns all employees in JSON format by reading in the input stream from the get-Json handler.
	 * @return - Returns all employees in JSON format
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 */

	private static StringBuffer getEmployees() throws IOException {

		StringBuffer response = new StringBuffer();
		
		try {
		URL url = new URL("http://localhost:7000/get-json");
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		String output;

		while ((output = reader.readLine()) != null) {
			response.append(output);
		}
		reader.close();
		} catch (IOException io) {
			System.out.println(io.getMessage());
		}
		return response;
		
		
	}
	
	
	/**
	 * Tests the functionality of the InsertEmployee handler.
	 * This method works through constructing a new employee object and converting it into a JSON string, which is then posted to the InsertEmployee handler on the server. 
	 * The JSON representation of the new employee is then inserted into the database.
	 * The method also returns information on the successfulness of the insert.
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 */

	private static void insertEmployee() throws IOException {
        Gson gson = new Gson();
		Employee emp = new Employee("Harold Marston", "M", "09-01-1974", "12 Lost Avenue", "SW1 8FG", 226, "Sales", "13-09-2012", 22000, "Harold.Marston@NMail.com");
		String intoJson = gson.toJson(emp);
		URL url;
		url = new URL("http://localhost:7000/InsertEmployee");
		
	
		// The connection object is created and configured:
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(15000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		// The employee in JSON format is then posted to the connection using output stream and buffered writer
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(intoJson);

	
		writer.flush();
		writer.close();
		
		os.close();
		String response = "";
		String line;
		int responseCode = conn.getResponseCode();
		System.out.println();
		System.out.println("Insert employee: ");
		System.out.println("responseCode = " + responseCode);
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				response += line;
			}
		} 
		System.out.println("response = " + response);
		System.out.println("Insert Successful!!");
		
	} 
	
	
	/**
	 * Tests the functionality of the GetSpecificEmployeeHandler.
	 * This method works by posting an employee number in hashmap format to the GetSpecificEmployee handler on the server and returning the specific employee in JSON format.
	 * The method also returns information on the successfulness of the retrieval.
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 */
	
	private static void	getSpecificEmployee() throws IOException {
		
		String urlParameters = "EmployeeNumber=226";
		URL url;
		url = new URL("http://localhost:7000/GetSpecificEmployee");
		
		// The connection object is created and configured:
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(15000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		// The employee number in hashmap format is then posted to the connection using output stream and buffered writer
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(urlParameters);

		writer.flush();
		writer.close();
		
		os.close();
		String response = "";
		String line;
		int responseCode = conn.getResponseCode();
		System.out.println();
		System.out.println("Return employee with the employee number of '226': ");
		System.out.println("responseCode = " + responseCode);
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				response += line;
			}
		} 
		System.out.println("response = " + response);
		System.out.println("Retrieved employee successfully!!");
		
	}
	

	/**
	 * Tests the functionality of the DeleteHandler.
	 * This method works by posting an employee number in hashmap format to the DeleteEmployee handler on the server, the employee number is then uses to identify the employee to be deleted. 
	 * The specified employee is then deleted from the database and information on the successfulness of the deletion is returned to the console.
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 */
	
	private static void deleteEmployee() throws IOException {

		String urlParameters = "EmployeeNumber=226";
		URL url;
		url = new URL("http://localhost:7000/DeleteEmployee");
		
		// The connection object is created and configured:
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(15000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("DELETE");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		// The employee number in hashmap format is then posted to the connection using output stream and buffered writer
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(urlParameters);

		
		writer.flush();
		writer.close();
	
		os.close();
		String response = "";
		String line;
		int responseCode = conn.getResponseCode();
		System.out.println();
		System.out.println("Delete employee with the employee number of '226': ");
		System.out.println("responseCode = " + responseCode);
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				response += line;
			}
		} 
		System.out.println("response = " + response);
		System.out.println("Employee deleted successfully!!");
		
	} 
	
	
	/**
	 * Tests the functionality of the UpdateEmployeeSalary handler.
	 * This method works by posting the field to be updated and the value of the update combined with the employee number in hashmap format to the UpdateEmployeeSalary handler on the server.
	 * The employee number is then used to identify the employee to be updated.
	 * The method also returns information on the successfulness of the update. 
	 * @throws IOException - This exception catches any potential io error that may occur from running the method.
	 */
			
	private static void UpdateEmployeeSalary() throws IOException {

		String urlParameters = "Salary=45000&EmployeeNumber=226";
		URL url;
		url = new URL("http://localhost:7000/UpdateEmployeeSalary");
		
		// The connection object is created and configured:
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(15000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("PUT");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		// The new salary value and employee number is then posted to the connection in hashmap format using output stream and buffered writer
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(urlParameters);

		
		writer.flush();
		writer.close();
		
		os.close();
		String response = "";
		String line;
		int responseCode = conn.getResponseCode();
		System.out.println();
		System.out.println("Update the salary of the employee with the employee number of '226': ");
		System.out.println("responseCode = " + responseCode);
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(conn.getInputStream(), Charset.forName("UTF-8")));
			while ((line = br.readLine()) != null) {
				response += line;
			}
		} 
		System.out.println("response = " + response);
		System.out.println("Employee Salary Updated successfully!!");
		
	} 
	
	

}
