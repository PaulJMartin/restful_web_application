import java.sql.*;
import java.util.*;

import com.google.gson.Gson;


/**
 * The purpose of this class is to test all of the methods within the ContactsDAO class.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class DatabaseTester {

	/**
	 * All EmployeeDAO functions are tested within the main method of this class. This is executed through use of all methods found within the EmployeeDAO class.
	 * @param args
	 */
	
	public static void main(String[] args) {
		
		EmployeeDAO empDao = new EmployeeDAO();
		Gson gson = new Gson();
		
		
		ArrayList<Employee> allEmployees = new ArrayList<Employee>();
		Employee selectedEmployee = null;
		Employee selectedEmployee2 = null;
		Employee selectedEmployee3 = null;
		Employee newEmployee = null;
		
		
	    
		
	    // The getAllEmployees method is used here to show the user all of the employees that are currently in the database. 
		
		/**
		 * Here the getAllEmployees method is called on the EmployeeDAO object, the values are then passed into an array list and converted into JSON format, before being sent to the console.
		 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving all employees from the database.
		 */
		
		System.out.println("Retrieving all employees from the database: ");
		
	    try {
	    	allEmployees = empDao.getAllEmployees();
			
		} catch (SQLException e) {			
			e.printStackTrace();
		}
				
		String intoJson = gson.toJson(allEmployees);
		System.out.println(intoJson);
		
		System.out.println();
		
		
		// The insertEmployees method is used here to test the functionality of the insert method.
  
		/**
		 * Here the insertEmployees method is called on the EmployeeDAO object, and the newly created employee is inserted the into the database.
		 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when inserting a new employee into the database.
		 */
		
		System.out.println("Inserting a new employee into the database");
		
		  try {
			  newEmployee =	new Employee("Elisha Trueman", "F", "09-01-1982", "12 Northumberland Road", "SW6 5FG", 22, "Finance", "13-09-2012", 32000, "Elisha.Trueman@NMail.com");
	    	empDao.insertEmployee(newEmployee);
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    }
	
		  System.out.println();
		  
        
		// The getEmployee method is used here to test that the insertEmployee method is working correctly, by attempting to retrieve the newly inserted employee. 
		  
		  
		 /**
		  * Here the getEmployee method is called on the EmployeeDAO object, the getEmployee method then takes a string representation of an employee number as its parameter and returns the employee which corresponds with that employee number. 
		  * The corresponding employee is then passed into a new employee object, before being converted into JSON format. The contents of the JSON string is then printed to the console. 
		  *  @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving a specific employee from the database.
		  */
		  
		  System.out.println("Retrieving the newly added employee from the database: ");
		  
		  try {
			  selectedEmployee = empDao.getEmployee("22");
				
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				String intoJson2 = gson.toJson(selectedEmployee);
				System.out.println(intoJson2);

				  System.out.println();   
		  
				  
		  
        // The updateEmployeeSalary is used here to test the functionality of the update method.
	
	    
		/**
		 * Here the updateEmployeeSalary method is called on the EmployeeDAO object, a string representation of the new salary value and the employee number of the employee to be updated is then entered into the methods parameter.
		 * The employee which corresponds with the specified employee number is then updated in the database. 
		 *  @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a particular field of a specific employee in the database.
		 */
				  
		System.out.println("Updating the salary of the employee with the employee number of 22");		  
				  
		 try {
			 empDao.updateEmployeeSalary("56000", "22");
				} catch (SQLException e) {
					  e.printStackTrace();
				}  
				  
		          System.out.println();
		          
		   
		          
		          
		// The getEmployee method is used here to test that the updateEmployeeSalary method is working correctly, by retrieving the newly updated employee.   
		          
	    /**
	     * Here the getEmployee method is called on the EmployeeDAO object, the getEmployee method then takes a string representation of an employee number as its parameter and returns the employee which corresponds with that employee number. 
	     * The corresponding employee is then passed into a new employee object, before being converted into JSON format. The contents of the JSON string is then printed to the console. 
		 *  @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving a specific employee from the database.
		 */  	          
		          
		          
		 System.out.println("Tests to see if update was a success by retrieving the employee with the employee number of 22: ");        
				  
	
	      try {
	    	  selectedEmployee2 = empDao.getEmployee("22");
	    	  
	    	 } catch (SQLException e) {
	    		 e.printStackTrace();
	    	 }
	
	      String intoJson3 = gson.toJson(selectedEmployee2);
			System.out.println(intoJson3);
			
			System.out.println();
			
			
			// The deleteEmployee method is used here to test the functionality of the delete method.
			
			
			/**
			 * Here the deleteEmployee method is called on the EmployeeDAO object, the deleteEmployee method then takes a string representation of an employee number as its parameter and deletes the employee which corresponds with that employee number. 
			 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when deleting a specific employee from the database.
			 */
			
			System.out.println("Deleting the employee with the employee number of 22");
			
			
			try { 
				empDao.deleteEmployee("22");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			System.out.println();
			
		
			
			/* The getEmployee method is used here to test that the delete method is working correctly, by attempting to retrieve the recently deleted employee.
			   If the delete method is working correctly, the statement should return a NULL value. */
			
			/**
			 * Here the getEmployee method is called on the EmployeeDAO object, the getEmployee method takes a string representation of an employee number as its parameter and returns the employee which corresponds with that employee number. 
			 * The corresponding employee is then passed into a new employee object, before being converted into JSON format. The contents of the JSON string is then printed to the console. 
			 *  @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving a specific employee from the database.
			 */         
			
			System.out.println("Tests to see if the delete was a success, by attempting to retrieve the employee with the employee number of 22: ");
			
			  try {
		    	  selectedEmployee3 = empDao.getEmployee("22");
		    	  
		    	 } catch (SQLException e) {
		    		 e.printStackTrace();
		    	 }
		
		      String intoJson4 = gson.toJson(selectedEmployee3);
				System.out.println(intoJson4);
				
				System.out.println();
				
	}
}
