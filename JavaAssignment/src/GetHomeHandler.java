
import java.io.IOException;
import java.io.OutputStream;




import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * The purpose of this class is to handle the GetHome request and to respond to that request by posting a welcome message to the user.
 * The get home handler acts as the home page of the server.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class GetHomeHandler implements HttpHandler {
	
	/**
	 * This method sends a welcome message to user to welcome them to the server.
	 * @exception IOException - This exception catches any potential io error that may occur from running the method.
	 */
	
	public void handle(HttpExchange httpEx) throws IOException {
		String response = "Welcome to my HTTP Server";  
		httpEx.sendResponseHeaders(200, response.length()); //HTTP 200 (OK)
		OutputStream outSt = httpEx.getResponseBody();
		outSt.write(response.getBytes()); // Sends the value of the String 'response' to the server.
		outSt.close();
	}
	

}
