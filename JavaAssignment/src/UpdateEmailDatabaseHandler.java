import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.HashMap;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


/**
 * The purpose of this class is to handle the UpdateEmployeeEmail request and to respond to that request by updating the appropriate field of the specific employee in the database.
 * @author Paul Martin, Student Number: 17097016
 * @version 1.0
 */

public class UpdateEmailDatabaseHandler implements HttpHandler{

	/**
	 * This method reads in an input from the user in the form of the new email value and the employee number of the employee to be be updated. The hashmap formatted data is then converted into string format and the value of the key/value pairs are converted into two String variables.
	 * The two string variables are then passed into the parameters of the updateEmployeeEmail method attached to an employeeDAO object, which in turn runs the method and updates the email of the specified employee in the database.
	 * @exception IOException - This exception catches any potential io error that may occur from running the method.
	 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when updating the email of the specific employee in the database.
	 */
	
	public void handle(HttpExchange httpEx) throws IOException {
		System.out.println();
		System.out.println("Updating employee: ");
		HashMap<String, String> update = new HashMap <String, String>();
	
		EmployeeDAO empDao = new EmployeeDAO();
		
		//Reading the request body.
		BufferedReader in = new BufferedReader(new InputStreamReader(httpEx.getRequestBody()));
		String line ="";
		String request = "";
		while ((line = in.readLine()) != null) {
			request = request + line;
		}
		System.out.println(">>>>>>>>>> "+request);
		
		
		String [] pairs = request.split("&");
		
		for (int i = 0; i<pairs.length; i++) {
			String pair = pairs[i];
			
			update.put(URLDecoder.decode(pair.split("=")[0],"UTF-8"),URLDecoder.decode(pair.split("=")[1],"UTF-8"));
		}

		//HashMap of posted data is now contained in the "update" variable.

		String Update = update.get("Email");
		String Id = update.get("EmployeeNumber");
		       
		
		System.out.println(Update);
		System.out.println(Id);
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(httpEx.getResponseBody()));					
		try { 
			empDao.updateEmployeeEmail(Update,Id); 
			httpEx.sendResponseHeaders(200, 0); //HTTP 200 (OK)
			out.write("Success!");
			out.close();
		}
		catch(SQLException se) {
			 httpEx.sendResponseHeaders(500, 0); //HTTP 500 (Internal Server Error)
			 out.write("Error Updating Employee");
			 out.close();
		}
	

	}

}
