import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * The purpose of this class is to handle the get-json request and to respond to that request by retrieving all employees from the database in JSON format.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class GetJsonHandler implements HttpHandler{
	
	/**
	 * This method retrieves all the employees from the database using the getAllEmployees method on an employeeDAO object and transfers the employee list to an array list.
	 * The array list containing the list of employees is converted into a JSON string and posted to the RESTClient.
	 *  @exception IOException - This exception catches any potential io error that may occur from running the method.
	 *  @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving all the employees from the database.
	 */

	public void handle(HttpExchange httpEx) throws IOException {
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(httpEx.getResponseBody()));
		ArrayList<Employee> allEmployees = new ArrayList<>();
		EmployeeDAO empDao = new EmployeeDAO();
		Gson gson = new Gson();
		System.out.println();
		System.out.println("Retrieving all employees: ");
		try {
			allEmployees = empDao.getAllEmployees();
			String myJson = gson.toJson(allEmployees);
			httpEx.sendResponseHeaders(200, 0); //HTTP 200 (OK)
			out.write(myJson); // Sends the string to the server.
			System.out.println(myJson);
			out.close();
		} catch (SQLException e) {
			System.out.println("SQL exception error generated, error = "+e.getMessage());
		}

	}

}
