
/**
 * The purpose of this class is to construct, modify and return an employee object. This class inherits from the person class.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */


public class Employee extends Person{
	
	private int employeeNumber;
	private String department;
	private String startDate;
	private float salary;
	private String email;
	
	/**
	 * Constructor method for constructing a person object. The parameters are assigned to the relevant instance variables within the method body.
	 * As the employee class inherits from the person class, the constructor parameters and methods of the person class can also be utilised within the employee class.
	 * The keyword 'super' is used to allow the employee constructor to access the parameters of the person constructor and construct an employee object using the person constructor parameters,
	 * as well as the employee constructor parameters. 
	 * @param n - Constructs the name of the employee object
	 * @param g - Constructs the gender of the employee object
	 * @param dateOfBirth - Constructs the date of birth of the employee object
	 * @param add - Constructs the address of the employee object
	 * @param pCode - Constructs the post code of the employee object
	 * @param empNum - Constructs the employee number of the employee object
	 * @param dep - Constructs the department of the employee object
	 * @param stDate - Constructs the start date of the employee object
	 * @param sal - Constructs the salary of the employee object
	 * @param em - Constructs the email of the employee object
	 */
	
	public Employee (String n, String g, String dateOfBirth, String add, String pCode, int empNum, String dep, String stDate, float sal, String em)
	{
		super(n, g, dateOfBirth, add, pCode);
		
		this.employeeNumber = empNum;
		this.department = dep;
		this.startDate = stDate;
		this.salary = sal;
		this.email = em;	
	}
	
    /**
     * Method for returning the employee number of the employee object
     * @return  Returns the value of the employee number variable
     */
	
	public int getEmployeeNumber()
	{
		return this.employeeNumber;
	}
	
	/**
	 * Method for returning the department of the employee object
	 * @return  Returns the value of the department variable
	 */
	
	public String getDepartment()
	{
		return this.department;
	}
	
	/**
	 * Method for returning the start date of the employee object
	 * @return  Returns the value of the start date variable
	 */
	
	public String getStartDate()
	{
		return this.startDate;
	}
	
	/**
	 * Method for returning the salary of the employee object
	 * @return  Returns the value of the salary variable
	 */
	
	public float getSalary()
	{
		return this.salary;
	}
	
	/**
	 * Method for returning the email of the employee object
	 * @return  Returns the value of the email variable
	 */
	
	public String getEmail()
	{
		return this.email;
	}
	
	
	
	
	/**
	 * Method for modifying the employee number of the employee object
	 * @param sEmpNum  Accepts a int input from the user and sets the employee number variable to the new value
	 */
	
	public void setEmployeeNumber(int sEmpNum)
	{
		employeeNumber = sEmpNum;
	}
	
	/**
	 * Method for modifying the department of the employee object
	 * @param sDep  Accepts a string input from the user and sets the department variable to the new value
	 */
	
	public void setDepartment(String sDep )
	{
		department = sDep;
	}
	
	/**
	 * Method for modifying the start date of the employee object
	 * @param sSDate  Accepts a string input from the user and sets the start date variable to the new value
	 */
	
	public void setStartDate(String sSDate)
	{
		startDate = sSDate;
	}
	
	/**
	 * Method for modifying the salary of the employee object
	 * @param sSal  Accepts a float input from the user and sets the salary variable to the new value
	 */
	
	public void setSalary(float sSal)
	{
		salary = sSal;
	}
	
	/**
	 * Method for modifying the email of the employee object
	 * @param sEM  Accepts a string input from the user and sets the email variable to the new value
	 */
	
	public void setEmail(String sEM)
	{
		email = sEM;
	}
	
	/**
	 * Method for returning a string representation of the employee object
	 * @return - Returns a string which includes a description of the variables and the values of the variables from the employee object.
	 */
	
	public String toString()
	{
		return "Employee name: "+super.getName()+", Gender: "+super.getGender()+", Date Of Birth: "+super.getDob()+", Address: "+super.getAddress()
		+", Postcode: "+super.getPostcode()+", Employee Number: "+this.getEmployeeNumber()+", Department: "+this.getDepartment()+", Start date: "+this.getStartDate()
		+", Salary: "+this.getSalary()+", Email: "+this.getEmail();
	}

}
