import java.sql.*;
import java.util.*;

/**
 * This class holds a method which creates a connection to an SQLite database and methods which allows the user to access and manipulate the data within the database. 
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */


public class EmployeeDAO {

	/**
	 * Method for creating the connection to the database.
	 * @return - Returns the database connection which holds the employees table.
	 * @exception ClassNotFoundException - This exception catches a class not found exception and informs the user that the driver class could not be found.
	 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving the connection to the employee database.
	 */
	
	public Connection getDBConnection() 
	{
		Connection dbConnection = null;
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			String dbURL = "jdbc:sqlite:empdb.sqlite";
			dbConnection = DriverManager.getConnection(dbURL);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
		
		
	}
	
	
	/**
	 * Method for returning all the employees from the employees table, using a 'SELECT * FROM employees' SQL statement. 
	 * After executing the statement, all the employees are held within an ArrayList and the array list is returned. 
	 * @return - returns all the employees from the employees table.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving all the employees from the database.
	 */
	
	public ArrayList<Employee> getAllEmployees() throws SQLException 
	{
		Connection dbConnection = null;
		Statement statement = null;
		ResultSet resultset = null;
		String query = "SELECT * FROM employees;";
		Employee temp = null;
		ArrayList<Employee> allEmployees = new ArrayList<>();
		
		try { 
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(query);
			resultset = statement.executeQuery(query);
			while (resultset.next()) {
				String name = resultset.getString("Name");
				String gender = resultset.getString("Gender");
				String dob = resultset.getString("DOB");
				String address = resultset.getString("Address");
				String postcode = resultset.getString("Postcode");
				
				int employeeNumber = resultset.getInt("EmployeeNumber");
				String department = resultset.getString("Department");
				String startDate = resultset.getString("StartDate");
				float salary = resultset.getFloat("Salary");
				String email = resultset.getString("Email");
				temp = new Employee (name, gender, dob, address, postcode, employeeNumber, department, startDate, salary, email);
				allEmployees.add(temp);
			}
				
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} finally {
				if(resultset != null)
				{
					resultset.close();
				}
				
				if(statement != null)
				{
				    resultset.close();	
				}
				if(dbConnection != null)
				{
					dbConnection.close();
				}
			}
		    return allEmployees;
			}
	
	
	
	
	/**
	 * Method for returning a specific employee. The employee is selected by their employee number using a SQL WHERE clause
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns the selected employee from the employees table.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when retrieving a specific employee from the database.
	 */
	
	
	public Employee getEmployee(String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		ResultSet resultset = null;
		String query = "SELECT * FROM employees WHERE EmployeeNumber = "+Id+";";
		Employee temp = null;
		
		try { 
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(query);
			resultset = statement.executeQuery(query);
			while (resultset.next())
			{
				String name = resultset.getString("Name");
				String gender = resultset.getString("Gender");
				String dob = resultset.getString("DOB");
				String address = resultset.getString("Address");
				String postcode = resultset.getString("Postcode");
				
				int employeeNumber = resultset.getInt("EmployeeNumber");
				String department = resultset.getString("Department");
				String startDate = resultset.getString("StartDate");
				float salary = resultset.getFloat("Salary");
				String email = resultset.getString("Email");
				temp = new Employee(name, gender, dob, address, postcode, employeeNumber, department, startDate, salary, email);
				
				}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if(resultset != null)
			{
				resultset.close();
			}
			
			if(statement != null)
			{
			    resultset.close();	
			}
			if(dbConnection != null)
			{
				dbConnection.close();
			}
		}
		
		return temp;
	}
	
	  
	
		/**
		 * Method for entering a new employee into the database, the new employee is passed into the parameter of the method when the method is called and get statements are used to pull the data into the SQL query, 
		 * which then updates the database.		
		 * @param emp - Accepts an employee object input from the user and uses the employee object values within the INSERT statement.
		 * @return - Returns true if the statement is executed correctly and false if not.
		 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when inserting a new employees into the database.
		 */
		
		public Boolean insertEmployee(Employee emp) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "INSERT INTO employees (Name, Gender, DOB, Address, Postcode, EmployeeNumber, Department, StartDate, Salary, Email)"
				     + " VALUES ('"+ emp.getName()+"', '"+ emp.getGender()+"', '"+emp.getDob()+"', '"+emp.getAddress()+"', '"+emp.getPostcode()+"', "
				     		+ ""+emp.getEmployeeNumber()+", '"+emp.getDepartment()+"', '"+emp.getStartDate()+"', "+emp.getSalary()+", '"+emp.getEmail()+"');";
		
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			
		System.out.println("Insert operation successful");
		return false;
		
	} 
		
	
		
		/**
		 * Method for updating the name of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
		 * @param feildUpdate - Accepts a string input from the user in the form of the new name value. The string is used to update the employees name.
		 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
		 * @return - Returns true if the statement is executed correctly and false if not.
		 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
		 */
		
		
	public Boolean updateEmployeeName(String feildUpdate, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Name = '"+feildUpdate+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
	   System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the gender of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new gender value. The string is used to update the employees gender.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeeGender(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Gender = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the date of birth of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new date of birth value. The string is used to update the employees date of birth.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeeDOB(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET DOB = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the address of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new address value. The string is used to update the employees address.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeeAddress(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Address = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the post code of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new post code value. The string is used to update the employees post code  .
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeePostcode(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Postcode = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the employee number of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new employee number value. The string is used to update the employees employee number.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employees in the database.
	 */
	
	public Boolean updateEmployeeEmployeeNumber(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET EmployeeNumber = "+Update+" WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the department of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new department value. The string is used to update the employees department.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	
	public Boolean updateEmployeeDepartment(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Department = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the start date of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new start date value. The string is used to update the employees start date.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	
	public Boolean updateEmployeeStartDate(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET StartDate = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the salary of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new salary value. The string is used to update the employees salary.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeeSalary(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Salary = "+Update+" WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for updating the email of a specific employee. The employee to be updated is selected by their employee number using a SQL WHERE clause.
	 * @param Update - Accepts a string input from the user in the form of the new email value. The string is used to update the employees email.
	 * @param Id - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when updating a specific employee in the database.
	 */
	
	public Boolean updateEmployeeEmail(String Update, String Id) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "UPDATE employees SET Email = '"+Update+"' WHERE EmployeeNumber = "+Id+";";
		try {
			dbConnection = getDBConnection();
			dbConnection.setAutoCommit(false);
			statement = dbConnection.createStatement();
			System.out.println(query);
			statement.executeUpdate(query);
			statement.close();
			dbConnection.commit();
			dbConnection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		System.out.println("Update operation successful");
				return false;
	}
	
	/**
	 * Method for deleting a specific employee. The employee to be deleted is selected by their employee number using a SQL WHERE clause.
	 * @param employeeNumber - Accepts a string input from the user in the form of an employee number and uses the string value within the Where clause of the SQL SELECT statement.
	 * @return - Returns true if the statement is executed correctly and false if not.
	 * @throws SQLException - This exception catches an SQLException and informs the user that there has been an error when deleting a specific employee from the database.
	 */
	
	public Boolean deleteEmployee(String employeeNumber) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String query = "DELETE FROM employees WHERE EmployeeNumber = "+employeeNumber+";";
        try {
        	dbConnection = getDBConnection();
        	dbConnection.setAutoCommit(false);
        	statement = dbConnection.createStatement();
        	System.out.println(query);
        	statement.executeUpdate(query);
        	statement.close();
        	dbConnection.commit();
        	dbConnection.close();
        } catch (SQLException e) {
        	System.out.println(e.getMessage());
        }
     System.out.println("Delete operation successful");
		return false;
	}
	

} 