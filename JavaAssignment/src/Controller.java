import java.io.IOException;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;

/**
 * The purpose of this class is to create a new httpServer object with a port number of 7000 and to create multiple pages which correspond with the relevant HTTP methods, such as POST, GET, PUT and DELETE.
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class Controller {
	
	/**
	 * The purpose of this main method is to create new server pages which make use of instances of the handler classes.
	 * @exception IOException - This exception catches an io error and informs the user that there has been a connection error and states said error.
	 */

	public static void main(String[] args) {
		
	
	try {
		HttpServer server = HttpServer.create(new InetSocketAddress(7000), 0); // Creates new httpServer object, running on port '7000'.
		server.createContext("/", new GetHomeHandler());
		server.createContext("/get-json", new GetJsonHandler()); 
		server.createContext("/InsertEmployee", new InsertDatabaseHandler()); 
		server.createContext("/DeleteEmployee", new DeleteHandler()); 
	    server.createContext("/UpdateEmployeeName", new UpdateNameDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeGender", new UpdateGenderDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeDOB", new UpdateDOBDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeAddress", new UpdateAddressDatabaseHandler()); 
	    server.createContext("/UpdateEmployeePostcode", new UpdatePostcodeDatabaseHandler());
	    server.createContext("/UpdateEmployeeEmployeeNumber", new UpdateEmployeeNumberDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeDepartment", new UpdateDepartmentDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeStartDate", new UpdateStartDateDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeSalary", new UpdateSalaryDatabaseHandler()); 
	    server.createContext("/UpdateEmployeeEmail", new UpdateEmailDatabaseHandler()); 
	    server.createContext("/GetSpecificEmployee", new GetSpecificEmployeeHandler()); 
		server.setExecutor(null);
		server.start(); //Starts the server
		System.out.println("Server running on port 7000"); // Informs user that the server is running on port '7000'.
	} catch (IOException io) {
		System.out.println("Connection issue found, issue: "+ io );
	}
		

	}
	}


