import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLException;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.google.gson.Gson;

/**
 * The purpose of this class is to handle the InsertEmployee request and to respond to that request by inserting the new employee into the database
 * @author Paul Martin, Student Number: 17097016.
 * @version 1.0
 */

public class InsertDatabaseHandler implements HttpHandler{

	/**
	 * This method reads in the new employee in JSON format, converts the input into an Employee object and passes the value into a new employee object.
	 * The employee object is then passed into the parameter of the insertEmployee method attached to an employeeDAO object, which in turn runs the method and inserts the new employee into the database.
	 * @exception IOException - This exception catches any potential io error that may occur from running the method.
	 * @exception SQLException - This exception catches an SQLException and informs the user that there has been an error when inserting the employee into the database.
	 */
	
	public void handle(HttpExchange httpEx) throws IOException {
		System.out.println();
		System.out.println("Adding new employee: ");
	    Gson gson = new Gson();
		EmployeeDAO empDao = new EmployeeDAO();
		
		//Reading the request body.
		BufferedReader in = new BufferedReader(new InputStreamReader(httpEx.getRequestBody()));
		String line ="";
		String request = "";
		while ((line = in.readLine()) != null) {
			request = request + line;
		}
		System.out.println(">>>>>>>>>> "+request);
	
		
		Employee e = gson.fromJson(request, Employee.class); // converts the JSON input into an employee and passes the employee into the newly created employee object.
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(httpEx.getResponseBody()));	
		out.write(request);
		
		try { 
			empDao.insertEmployee(e); 
			httpEx.sendResponseHeaders(200, 0); //HTTP 200 (OK)
			out.write(" - Employee Insert Successful!");
			out.close();
		}
		catch(SQLException se) {
			 httpEx.sendResponseHeaders(500, 0); //HTTP 500 (Internal Server Error)
			 out.write("Error Inserting Employee");
			 out.close();
		}
		
		finally {
			out.close();
		}
	

	}

}
