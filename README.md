# RESTful_Web_Application

RESTful web service in Java using HTTP Server which allows a user to make CRUD (i.e. Create, Retrieve, Update and Delete) database operations on a back-end SQLite database storing fictitious employee data.

The default URL is http://localhost:7000/.
The JavaScript Object Notation (JSON) format is used for data exchange. 
CRUD operations are accessible through the following structured URLs:

http://localhost:7000/get-json (Returns all employee information stored in the database in JSON format.)

http://localhost:7000/InsertEmployee (Inserts a new employee)

http://localhost:7000/DeleteEmployee (Deletes an employee)

http://localhost:7000/UpdateEmployeeName (Updates the employee name of a specific employee)

http://localhost:7000/UpdateEmployeeGender (Updates the gender of a specific employee)

http://localhost:7000/UpdateEmployeeDOB (Updates the DOB of a specific employee)

http://localhost:7000/UpdateEmployeeAddress (Updates the address of a specific employee)

http://localhost:7000/UpdateEmployeePostcode (Updates the post code of a specific employee)

http://localhost:7000/UpdateEmployeeEmployeeNumber (Updates the employee number of a specific employee)

http://localhost:7000/UpdateEmployeeDepartment (Updates the department of a specific employee)

http://localhost:7000/UpdateEmployeeStartDate (Updates the start date of a specific employee)

http://localhost:7000/UpdateEmployeeSalary (Updates the salary of a specific employee)

http://localhost:7000/UpdateEmployeeEmail (Updates the employee email of a specific employee)

http://localhost:7000/GetSpecificEmployee (Returns a specific employee by employee number)
